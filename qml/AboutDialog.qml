import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog {
  id: dialog
  title: i18n.tr("About")

  Label {
    width: parent.width
    wrapMode: Text.WordWrap
    text: i18n.tr("A tutorial shoppinglist app remix for Jelkner to learn from and use.")
  }

  Button {
    text: i18n.tr("Close")
    onClicked: PopupUtils.close(dialog)
  }
}
