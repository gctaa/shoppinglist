/*
 * Copyright (C) 2022  Jeffrey Elkner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * shoppinglist is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.LocalStorage 2.7
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Qt.labs.settings 1.0

MainView {
  id: root
  objectName: 'mainView'
  applicationName: 'shoppinglist.jelkner'
  automaticOrientation: true

  width: units.gu(45)
  height: units.gu(75)

  backgroundColor: UbuntuColors.graphite

  property bool selectionMode: false 
  property bool showAquired: false
  property string dbName: "ShoppingListDB"
  property string dbVersion: "1.0"
  property string dbDescription: "Database for Jelkner's shopping list"
  property int dbEstimatedSize: 10000
  property var db: LocalStorage.openDatabaseSync(
    dbName, dbVersion, dbDescription, dbEstimatedSize
  )
  property string shoppingListTable: "ShoppingList"

  function initializeShoppingList() {
    db.transaction(function(tx) {
      var sql = 'CREATE TABLE IF NOT EXISTS ' + shoppingListTable;
      sql += ' (name TEXT, acquired BOOLEAN, selected BOOLEAN)';
      tx.executeSql(sql);
      sql = 'SELECT rowid, name, acquired, selected FROM ' + shoppingListTable;
      var results = tx.executeSql(sql);

      // Update ListModel
      for (var i=0; i<results.rows.length; i++) {
        shoppinglistModel.append({
          "rowid": results.rows.item(i).rowid,
          "name": results.rows.item(i).name,
          "acquired": Boolean(results.rows.item(i).acquired),
          "selected": Boolean(results.rows.item(i).selected)
        });
      }
    })
  }

  ListModel {
    id: shoppinglistModel

    function addItem(name, selected) {
      db.transaction(function(tx) {
        var sql = 'INSERT INTO ' + shoppingListTable
        sql += ' (name, acquired, selected) VALUES(?, ?, ?)'
        var result = tx.executeSql(sql, [name, false, selected]);
        shoppinglistModel.append(
          {
            "rowid": Number(result.insertId),
            "name": name,
            "acquired": false,
            "selected": selected
          });
      })
    }

    function removeItem(index) {
      var rowid = shoppinglistModel.get(index).rowid;
      db.transaction(function(tx) {
        var sql = 'DELETE FROM ' + shoppingListTable + ' WHERE rowid=?';
        tx.executeSql(sql, [rowid]);
      })
      shoppinglistModel.remove(index);
    }

    function removeSelectedItems() {
      for (var i=shoppinglistModel.count-1; i>=0; i--) {
        if (shoppinglistModel.get(i).selected)
          shoppinglistModel.removeItem(i);
      }
    }

    function removeAllItems() {
      db.transaction(function(tx) {
        tx.executeSql('DELETE FROM ' + shoppingListTable);
      })
      shoppinglistModel.clear();
    }
  }

  Component {
    id: removeAllDialog

    OKCancelDialog {
      title: i18n.tr("Remove all items")
      text: i18n.tr("Are you sure?")
      onDoAction: shoppinglistModel.removeAllItems()
      onCancelAction: root.selectionMode = false
    }
  }

  Component {
    id: removeSelectedDialog

    OKCancelDialog {
      title: i18n.tr("Remove selected items")
      text: i18n.tr("Are you sure?")
      onDoAction: {
        shoppinglistModel.removeSelectedItems();
        root.selectionMode = false;
      }
      onCancelAction: root.selectionMode = false
    }
  }

  Component {
    id: aboutDialog
    AboutDialog {}
  }

  Page {
    anchors.fill: parent

    Component.onCompleted: initializeShoppingList()

    header: PageHeader {
      id: header
      title: i18n.tr('Jelkner\'s Shopping List')
      subtitle: i18n.tr('Never forget what to buy')

      ActionBar {
        anchors {
          top: parent.top
	  right: parent.right
	  topMargin: units.gu(1)
          rightMargin: units.gu(1)
        }
        numberOfSlots: 0
        actions: [
          Action {
            iconName: root.showAquired ? "view-off" : "view-on"
            text: i18n.tr("Show/Hide Aquired")
            onTriggered: {
              root.showAquired = !root.showAquired
              shoppinglistView.refresh();
            }
          },
          Action {
            iconName: "erase"
            text: i18n.tr("Remove all...")
            onTriggered: PopupUtils.open(removeAllDialog)
          },
          Action {
            iconName: "erase"
            text: i18n.tr("Remove selected...")
            onTriggered: PopupUtils.open(removeSelectedDialog)
          },
          Action {
            iconName: "info"
            text: i18n.tr("About")
            onTriggered: PopupUtils.open(aboutDialog)
          }
        ]
      }

      StyleHints {
        foregroundColor: UbuntuColors.orange
        backgroundColor: UbuntuColors.jet
        dividerColor: UbuntuColors.inkstone
      }
    }

    Button {
      id: buttonAdd
      anchors {
        top: header.bottom
        right: parent.right
        topMargin: units.gu(1)
        rightMargin: units.gu(1)
      }
      text: i18n.tr('Add')
      color: UbuntuColors.silk
      onClicked: {
        // Ignore empty items (added by jelkner, not in tutorial)
        if (textFieldInput.text != "") 
          shoppinglistModel.addItem(textFieldInput.text, false); 
        textFieldInput.text = "";
      }
    }

    TextField {
      id: textFieldInput
      anchors {
        top: header.bottom
        left: parent.left
        right: buttonAdd.left
        topMargin: units.gu(1)
        leftMargin: units.gu(1)
        rightMargin: units.gu(1)
      }
      placeholderText: i18n.tr('Shopping list item')
    }

    ListView {
      id: shoppinglistView
      anchors {
        top: textFieldInput.bottom
        bottom: parent.bottom
        left: parent.left
        right: parent.right
        topMargin: units.gu(1)
      }
      model: shoppinglistModel

      delegate: ListItem {
        width: parent.width
        height: units.gu(4)
        Rectangle {
          anchors.fill: parent
          visible: !acquired || root.showAquired
          color: UbuntuColors.silk
          CheckBox {
            id: itemCheckbox
            visible: root.selectionMode
            checked: shoppinglistModel.get(index).selected
            anchors {
              left: parent.left
              leftMargin: units.gu(1)
              verticalCenter: parent.verticalCenter
            }
            onClicked: {
              if (root.selectionMode) {
                var currentState = shoppinglistModel.get(index).selected
                shoppinglistModel.get(index).selected = !currentState
                shoppinglistView.refresh();
              }
            }
          }
          Text {
            id: itemText
            text: name
            anchors {
              left: root.selectionMode ? itemCheckbox.right : parent.left
              leftMargin: root.selectionMode ? units.gu(1) : units.gu(2)
              verticalCenter: parent.verticalCenter
            }
            color: "black" 
          }
          MouseArea {
            anchors.fill: itemText
            onPressAndHold: root.selectionMode = true
          }
          CheckBox {
            id: foundCheckbox
            checked: shoppinglistModel.get(index).acquired
            anchors {
              right: parent.right
              rightMargin: units.gu(2)
              verticalCenter: parent.verticalCenter
            }
            onClicked: {
              var currentState = shoppinglistModel.get(index).acquired
              shoppinglistModel.get(index).acquired = !currentState
              shoppinglistView.refresh();
            }
          }
        }
        leadingActions: ListItemActions {
          actions: [
            Action {
              iconName: "delete"
              onTriggered: shoppinglistModel.removeItem(index)
            }
          ]
        }
      }
      function refresh() {
        // Refresh the list to update the selected status
        var tmp = model;
        model = null;
        model = tmp;
      }
    }
  }
}
